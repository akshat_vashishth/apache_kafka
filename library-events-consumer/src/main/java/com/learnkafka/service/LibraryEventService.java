package com.learnkafka.service;

import java.util.Objects;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.entity.LibraryEvent;
import com.learnkafka.jpa.LibraryEventRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LibraryEventService {

    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private LibraryEventRepository libraryEventRepository;
    
    public void processLibraryEvent(ConsumerRecord<Integer, String> consumerRecord) throws JsonMappingException, JsonProcessingException {
        
        LibraryEvent libraryEvent = objectMapper.readValue(consumerRecord.value(), LibraryEvent.class);
        log.info(" LibraryEvent is {} ", libraryEvent);
        
        switch(libraryEvent.getLibraryEventType()) {
            case NEW:
                save(libraryEvent);
                break;
            case UPDATE:
                validate(libraryEvent);
                save(libraryEvent);
                break;
            default:
                log.info(" Invalid Library Event Type {} ", libraryEvent.getLibraryEventType());
        }
    }

    private void save(LibraryEvent libraryEvent) {
        libraryEvent.getBook().setLibraryEvent(libraryEvent);
        libraryEventRepository.save(libraryEvent);
        log.info("Successfully Persisted the library Event {} ", libraryEvent);
    }

    private void validate(LibraryEvent libraryEvent) {
        
        Integer libraryEventId = libraryEvent.getLibraryEventId();
        if (Objects.isNull(libraryEventId)) {
            throw new IllegalArgumentException("LibraryEventId is missing");
        }  
        
        libraryEventRepository.findById(libraryEventId)
            .orElseThrow(() -> new IllegalArgumentException("Not a valid LibraryEventId = " + libraryEventId));
        log.info("Validation Successful for Library Event : {} ", libraryEventId);
    }
    
}
