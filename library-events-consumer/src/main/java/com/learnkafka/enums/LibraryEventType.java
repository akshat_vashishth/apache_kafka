package com.learnkafka.enums;

public enum LibraryEventType {
    NEW, UPDATE
}
