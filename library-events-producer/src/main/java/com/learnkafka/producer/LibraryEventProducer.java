package com.learnkafka.producer;

import java.util.List;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.domain.LibraryEvent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LibraryEventProducer {
    
    @Autowired
    private KafkaTemplate<Integer,String> kafkaTemplate;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    String topic = "library-events";

    public void sendLibraryEvent(LibraryEvent libraryEvent) throws JsonProcessingException{
        Integer key = libraryEvent.getLibraryEventId();
        String message = objectMapper.writeValueAsString(libraryEvent);
        ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.sendDefault(key, message);
        listenableFuture.addCallback(invokeCallback(key, message));
    }
    
    public void sendLibraryEvent_Approach2(LibraryEvent libraryEvent) throws JsonProcessingException{
        Integer key = libraryEvent.getLibraryEventId();
        String message = objectMapper.writeValueAsString(libraryEvent);
        //ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(topic, key, message);
        ProducerRecord<Integer, String> roducerRecords = buildProducerRecords(topic, key, message);
        ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send(roducerRecords);
        listenableFuture.addCallback(invokeCallback(key, message));
    }

    private ListenableFutureCallback<SendResult<Integer, String>> invokeCallback(Integer key, String message) {
        return new ListenableFutureCallback<SendResult<Integer, String>>() {

            @Override
            public void onSuccess(SendResult<Integer, String> result) {
                handleSuccess(key,message, result);
            }

            @Override
            public void onFailure(Throwable exception) {
                handleFailure(key,message, exception);
            }
        };
    }

    protected void handleSuccess(Integer key, String message, SendResult<Integer, String> result) {
        log.info("Message Sent Successfully for the key : {} and the message is {} for partition is {} ", 
                key, message, result.getRecordMetadata().partition());
    }
    
    protected void handleFailure(Integer key, String message, Throwable exception) {
        log.error("Error sending Message for the key : {} and the message is {} for exception is {} ", 
                key, message, exception.getMessage());
    }
    

    protected ProducerRecord<Integer, String> buildProducerRecords(String topic2, Integer key, String message) {
        List<Header> recordHeader = List.of(new RecordHeader("event-source", "scanner".getBytes()));
        return new ProducerRecord<Integer, String>(topic , null , key, message, recordHeader);
    }

}
